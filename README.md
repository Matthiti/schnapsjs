# schnapsjs

This is an implementation of the zk-SCHNAPS protocol described in [\[Roe22\]](http://essay.utwente.nl/93502/).

## Install

```bash
npm install schnapsjs
```

## Usage

### Create a registration proof

```js
import { readFileSync } from 'fs';
import { register, util } from 'schnapsjs';

const saverPk = JSON.parse(readFileSync('/path/to/saver_pk.json', 'utf-8'));
const password = 7521150648595452032677760325358n; // M1sch13f m@n@g3d
const salt = util.generateSalt();
const { proof, publicSignals, ciphertext } = await register.createProof(
    { password, salt: util.generateRandomSalt() },
    '/path/to/circuit.wasm',
    '/path/to/circuit.zkey',
    saverPk
);
```

### Verify a registration proof

```js
import { readFileSync } from 'fs';
import { register } from 'schnapsjs';

const { proof, publicSignals, ciphertext } = ...; // Generated using register.createProof
const verificationKey = JSON.parse(readFileSync('/path/to/verification_key.json', 'utf-8'));
const saverPk = JSON.parse(readFileSync('/path/to/saver_pk.json', 'utf-8'));
const correctProof = await register.verifyProof(
    verificationKey,
    saverPk,
    ciphertext,
    publicSignals,
    proof
);
```

### Create a login encryption

```js
import { readFileSync } from 'fs';
import { login } from 'schnapsjs';

const saverPk = JSON.parse(readFileSync('/path/to/saver_pk.json', 'utf-8'));
const passwordHash = ...; // Calculate the hash of the password with the same salt as during registration
const ciphertext = login.createEncryption(
    saverPk,
    passwordHash
);
```

### Verify a login encryption

```js
import { readFileSync } from 'fs';
import { login } from 'schnapsjs';

const saverPk = JSON.parse(readFileSync('/path/to/saver_pk.json', 'utf-8'));
const ciphertext = ...; // Generated using login.createEncryption
const correctEncryption = await login.verifyEncryption(
    saverPk,
    ciphertext
);
```

### Check if two encryptions are equal

```js
import { readFileSync } from 'fs';
import { login } from 'schnapsjs';

const saverSk = JSON.parse(readFileSync('/path/to/saver_sk.json', 'utf-8'));
const saverVk = JSON.parse(readFileSync('/path/to/saver_vk.json', 'utf-8'));
const registerCiphertext = ...; // Generated using register.createProof
const loginCiphertext = ...; // Generated using login.createEncryption
const areEqual = await login.compareEncryptions(
    saverSk,
    saverVk,
    registerCiphertext,
    loginCiphertext
);
```
