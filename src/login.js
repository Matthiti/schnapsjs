import { saver } from 'snarkjs';
import { getCurveFromName, utils } from 'ffjavascript';
import { SHA3 } from 'sha3';

async function createEncryption(_saverPk, passwordHash) {
  const saverPk = utils.unstringifyBigInts(_saverPk);

  const curve = await getCurveFromName(saverPk.curve);
  const Fr = curve.Fr;
  const G1 = curve.G1;

  const pk_X_i = G1.fromObject(saverPk.X[0]);
  const G_i = G1.fromObject(saverPk.G_is[0]);

  const r = Fr.random();
  const ciphertext = await saver.encrypt(saverPk, [passwordHash], r);

  const k_1 = Fr.random();
  const k_2 = Fr.random();

  const commitment = G1.toAffine(G1.add(
      G1.timesFr(pk_X_i, k_1),
      G1.timesFr(G_i, k_2)
  ));

  const challenge = Fr.e(BigInt('0x' + new SHA3(256)
      .update(G1.toString(G1.toAffine(pk_X_i)))
      .update(G1.toString(G1.toAffine(G_i)))
      .update(G1.toString(G1.toAffine(G1.fromObject(ciphertext.c[0]))))
      .update(G1.toString(commitment))
      .digest('hex')
  ));

  const response = [
      Fr.toObject(Fr.add(k_1, Fr.mul(r, challenge))),
      Fr.toObject(Fr.add(k_2, Fr.mul(Fr.e(passwordHash), challenge)))
  ];

  return utils.stringifyBigInts({
    ...ciphertext,
    phi: {
      commitment: G1.toObject(commitment),
      challenge: Fr.toObject(challenge),
      response
    }
  });
}

async function verifyEncryption(_saverPk, _ciphertext) {
  const saverPk = utils.unstringifyBigInts(_saverPk);
  const ciphertext = utils.unstringifyBigInts(_ciphertext);

  if (!await saver.verifyEncryption(saverPk, ciphertext)) {
    return false;
  }

  const curve = await getCurveFromName(saverPk.curve);
  const Fr = curve.Fr;
  const G1 = curve.G1;

  const pk_X_i = G1.fromObject(saverPk.X[0]);
  const G_i = G1.fromObject(saverPk.G_is[0]);
  const ct_c_i = G1.fromObject(ciphertext.c[0]);
  const phi_challenge = Fr.fromObject(ciphertext.phi.challenge);
  const phi_response = ciphertext.phi.response.map(r => Fr.fromObject(r));

  const hash = Fr.e(BigInt('0x' + new SHA3(256)
      .update(G1.toString(G1.toAffine(pk_X_i)))
      .update(G1.toString(G1.toAffine(G_i)))
      .update(G1.toString(G1.toAffine(ct_c_i)))
      .update(G1.toString(G1.toAffine(
          G1.sub(
              G1.add(
                  G1.timesFr(pk_X_i, phi_response[0]),
                  G1.timesFr(G_i, phi_response[1])
              ),
              G1.timesFr(ct_c_i, phi_challenge)
          )
      )))
      .digest('hex')
  ));

  return Fr.eq(hash, phi_challenge);
}

async function compareEncryptions(saverSk, saverVk, _ct_0, _ct_1) {
  const ct_0 = utils.unstringifyBigInts(_ct_0);
  const ct_1 = utils.unstringifyBigInts(_ct_1);

  const curve = await getCurveFromName(saverVk.curve);
  const G1 = curve.G1;
  const Gt = curve.Gt;

  const ct = {
    c_0: G1.toObject(G1.toAffine(G1.sub(
        G1.fromObject(ct_0.c_0),
        G1.fromObject(ct_1.c_0)
    ))),
    c: [
        G1.toObject(G1.toAffine(G1.sub(
            G1.fromObject(ct_0.c[0]),
            G1.fromObject(ct_1.c[0])
        )))
    ]
  };

  const { m } = await saver.decrypt(saverSk, saverVk, ct);
  return Gt.eq(Gt.fromObject(m[0]), Gt.zero);
}

export {
  createEncryption,
  verifyEncryption,
  compareEncryptions
}
