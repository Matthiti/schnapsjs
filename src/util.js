import crypto from 'crypto';

function generateRandomSalt(bytes = 32) {
  const byteArray = new Uint8Array(bytes);
  (crypto.webcrypto || window.crypto).getRandomValues(byteArray);
  const bytesHex = byteArray.reduce((acc, cur) => acc + ('00' + cur.toString(16)).slice(-2), '');
  return BigInt('0x' + bytesHex);
}

function encodePassword(password, mappingFunction, base) {
  return password.split('')
      .map(c => mappingFunction(c))
      .reduce((acc, cur , index) => acc + BigInt(cur) * BigInt(base) ** BigInt(index), 0n);
}

export {
  generateRandomSalt,
  encodePassword
}
