import * as register from './register.js';
import * as login from './login.js';
import { readFileSync } from 'fs';

async function run() {
  const saverPk = JSON.parse(readFileSync('/Users/matthijs/Documents/UT/Master/Thesis/Final Project/SQ1/Code/saver_pk.json', 'utf8'));
  const saverSk = JSON.parse(readFileSync('/Users/matthijs/Documents/UT/Master/Thesis/Final Project/SQ1/Code/saver_sk.json', 'utf8'));
  const saverVk = JSON.parse(readFileSync('/Users/matthijs/Documents/UT/Master/Thesis/Final Project/SQ1/Code/saver_vk.json', 'utf8'));

  const password = 12345678901234567890n;
  const passwordHash = 19741172798469405326036163088375756298795084232788896636400604710902118892712n;

  const { proof, publicSignals, ciphertext: registerCiphertext } = await register.createProof(
      {
        password,
        salt: 456
      },
      '/Users/matthijs/Documents/UT/Master/Thesis/Final Project/SQ1/Code/circuit/target/circuit_js/circuit.wasm',
      '/Users/matthijs/Documents/UT/Master/Thesis/Final Project/SQ1/Code/setup/circuit/circuit_final.zkey',
      saverPk
  );

  const correctRegisterProof = await register.verifyProof(
      JSON.parse(readFileSync('/Users/matthijs/Documents/UT/Master/Thesis/Final Project/SQ1/Code/setup/verification_key.json', 'utf8')),
      saverPk,
      registerCiphertext,
      publicSignals,
      proof
  );

  console.log(`Correct registration proof: ${boolToString(correctRegisterProof)}`);

  const loginCiphertext = await login.createEncryption(
      saverPk,
      passwordHash
  );

  const correctLoginProof = await login.verifyEncryption(
      saverPk,
      loginCiphertext
  );

  console.log(`Correct login proof: ${boolToString(correctLoginProof)}`);

  const correctPassword = await login.compareEncryptions(
      saverSk,
      saverVk,
      registerCiphertext,
      loginCiphertext
  );

  console.log(`Correct password: ${boolToString(correctPassword)}`)
}

function boolToString(bool) {
  return bool
    ? 'yes ✅ '
    : 'no ❌ ';
}

run().then(() => process.exit(0));
