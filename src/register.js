import { saver } from 'snarkjs';
import { utils } from 'ffjavascript';

async function createProof(input, wasmFile, zkeyFileName, saverPk) {
  return utils.stringifyBigInts(await saver.encryptThenProve(input, wasmFile, zkeyFileName, saverPk))
}

function verifyProof(vk_verifier, saverPk, ciphertext, publicSignals, proof) {
  return saver.verifyEncryptionAndProof(vk_verifier, saverPk, ciphertext, publicSignals, proof);
}

export {
  createProof,
  verifyProof
}
